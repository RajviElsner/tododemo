const initState = [];

const toDoListReducer = (state = initState, action) => {
    switch (action.type) {
        case 'ADD_TASK':
            return [...state, action.tasks];

        case 'DEL_TASK':
            return state.filter(({id}) => id !== action.id);

        default:
            return state;
    }
};
export default toDoListReducer;