import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { addTaskList } from '../redux/actions/taskActions';
import { v4 as uuidv4 } from 'uuid';
import { Button, Typography, Input } from 'antd';
import "antd/dist/antd.css";
const { Title } = Typography;
const AddTask = (props) => {
    const dispatch = useDispatch();
    const [taskText, setTaskText] = useState('');
    let id;

    const addTask = (e) => {
        e.preventDefault();
        if (taskText !== '') {
            id = uuidv4();
            setTaskText('');
            dispatch(addTaskList({ id, taskText }));
        } else {
            alert("Fill the required field");
        }
    }
    // onClick={props.history.push('./display')}
    return (
        <>
        <br /><br />
            <center>
                <Title level={2}>Add New Task</Title>
                <Input style={{width:'40%'}}
                    type="text" placeholder='enter task'
                    value={taskText} onChange={e => setTaskText(e.target.value)} 
                /><br /><br />
                <Button type='primary' onClick={addTask}>
                    Add Task
                </Button><br /><br />
                <Button onClick={()=>props.setDispAll(true)}>Show all Tasks</Button>
            </center>
        </>

    )
};

export default AddTask;