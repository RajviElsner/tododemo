import { useState } from 'react';
const ImgUpload = () => {
    const [image,setImage]=useState(null);
    /*const fileSelected = (e) =>{ 
        e.preventDefault();
        setImage(e.target.files[0]);
        console.log(e.target.files[0]);
    }*/

    const fileUpload = () => {
        console.log(image);
    }

    return(
        <>
            <center>
                <h3>Image Upload</h3>
                <input type='file' onChange={(e)=>setImage(e.target.files[0])}/>
                <button onClick={fileUpload}>Upload</button>
            </center>
        </>
    )
}

export default ImgUpload;