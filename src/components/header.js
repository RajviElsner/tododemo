import React from 'react';
import '../App.css';
import { Layout, Menu, Typography } from 'antd';

const { Header } = Layout;
const { Title } = Typography;
export class HeaderCom extends React.Component {
    render() {

        return (
            <Layout>
                <Header>
                    <Menu theme="dark" mode="horizontal">
                        <Title level={3} style={{ color: 'white', padding: 10,marginLeft:'45%' }}>ToDo App</Title>
                    </Menu>
                </Header>
            </Layout>
        )
    }
};

