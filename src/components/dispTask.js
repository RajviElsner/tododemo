import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { delTask } from '../redux/actions/taskActions';
import { Button, Typography, Row, Col, Space } from 'antd';
import { CheckOutlined } from '@ant-design/icons';
const { Title } = Typography;
function DispTask() {
    const dispatch = useDispatch();
    const [completed_tasks, setCompleted_tasks] = useState([]);
    const [showCTasks, setCTasks] = useState(false);
    const [showActiveTasks, setActiveTasks] = useState(false);
    const alltasks = useSelector((state) => state.tasks);
    const ActiveTasks = alltasks.slice(completed_tasks);

    const remove = (task) => {
        completed_tasks.push(task);
        //console.log(completed_tasks);
        dispatch(delTask(task.data.id));
    }

    return (
        <><br /><br />
            <center>

                <Title level={2}>All Tasks</Title>
                {alltasks.length > 0 ? (
                    //disp
                    alltasks.map(data => (
                        <div>
                            <li>{data.taskText}
                                <Button type='link' onClick={() => remove({ data })}>
                                    Done<CheckOutlined />
                                </Button></li>
                        </div>
                    ))
                ) : (
                    <p>No Tasks to display</p>
                )}<br /><br />
                <Row>
                    <Col span={12}>
                        <Button type="link" onClick={() => setActiveTasks(true)}>Show active Tasks</Button><br />
                        {showActiveTasks ? (
                            ActiveTasks.map(data => (
                                <p>{data.taskText}</p>
                            ))
                        ) : null}
                    </Col>
                    <Col span={12}>
                        <Button type="link" onClick={() => setCTasks(true)}>Show completed Tasks</Button><br />
                        {showCTasks ? (
                            completed_tasks.length > 0 ? (
                                completed_tasks.map(task => (
                                    <p>{task.data.taskText}<CheckOutlined /></p>
                                ))
                            ) : (
                                <p>No tasks completed yet</p>
                            )
                        ) : null}



                    </Col>
                </Row>
            </center>
        </>
    )
};

export default DispTask;

